from flask import Flask, render_template, json, request
from datetime import datetime
import threading
from sensorreader import *
import os, csv
import glob
import time

MIN = 60
DEBUG = True
app = Flask(__name__)
temperatures = list()

def getFormatedDatetime(date, timestamp):
    currDateTime = datetime.strptime(date + ' '+ timestamp, "%Y-%m-%d %H:%M:%S")
    return currDateTime.strftime('%d %b, %H:%M')


def filterTemperatures(rows, interval):
    times = list()
    temps = list()
    prev_time = None
    firstRow = rows[0]

    formatedDatetime = getFormatedDatetime(firstRow['date'], firstRow['time'])
    times.append(formatedDatetime)
    temps.append(firstRow['temperature'])
    for row in rows:      
        date =  row['date']
        timestamp = row['time']
        temp = row['temperature']
        curr_time = datetime.strptime(timestamp, "%H:%M:%S")
        
        if(prev_time is None):
            prev_time = curr_time
        
        diff = curr_time - prev_time

        if(diff.seconds > MIN*interval -5 or diff.seconds > MIN*interval):
            formatedDatetime = getFormatedDatetime(date, timestamp)
            times.append(formatedDatetime)
            temps.append(temp)     
            prev_time = curr_time

    if(DEBUG): print("Temps amount result: " + str(len(temps)))
    return temps, times


def getRecentTemperatures(noOfRows, interval):
    with open("logs/temperatures.csv", 'r') as f:
        reader = csv.DictReader(f, fieldnames=['date', 'time', 'temperature'])
        next(f)

        rows = list(reader)
        rowsToFilter = noOfRows * int(interval/2)

        if(DEBUG):
            print("Rows total: " + str(len(rows)))
            print("Rows to filter: " + str(rowsToFilter))
            print("Interval: " + str(interval))
        temps, times = filterTemperatures(rows[-rowsToFilter:], interval)

    return {'times': times, 'temps': temps}

@app.route('/')
def index():
    temp = read_temp()
    return render_template('index.html', degrees = temp)

@app.route('/get-temp')
def getNewTemp():
    temp = read_temp()
    return json.jsonify(temp)

@app.route('/update-temp-chart', methods=['POST'])
def updateTempChart():
    try:
        noOfPoints = int(request.form['noOfPoints'])
        intervalMinute = int(request.form['intervalMinute'])
        newTemps = getRecentTemperatures(noOfPoints, intervalMinute)
        return json.jsonify(newTemps)
    except:
        pass


if __name__ == '__main__':  
    app.run(debug=True, host='0.0.0.0')
