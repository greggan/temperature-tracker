import sched, time
import csv
from sensorreader import *


class TempCommunicator:

    while True:
        with open("logs/temperatures.csv", 'a') as f:
            temp = read_temp()
            writer = csv.DictWriter(
                f, fieldnames=['date', 'time', 'temperature'])

            if os.stat("logs/temperatures.csv").st_size == 0:
                writer.writeheader()

            date = time.strftime("%Y-%m-%d")
            timeNow = time.strftime("%H:%M:%S")
            newItem = {'date': date, 'time': timeNow, 'temperature': temp}
            print(newItem)
            writer.writerow(newItem)

        time.sleep(119)
