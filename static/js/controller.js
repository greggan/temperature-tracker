// import * as Chart from '../Chart.min';

setInterval(() => {
    getCurrentTemp();
}, 5000);



function getCurrentTemp() {
	$.get('/get-temp', temp =>{
		$("#degrees").html(temp);
	});
}

async function updateTempChart(noOfPoints, intervalMinute) {
	return await $.post('/update-temp-chart', {'noOfPoints': noOfPoints, 'intervalMinute': intervalMinute}, response =>{
		return response;
	});
}
