import csv
import time
from datetime import datetime, timedelta
MIN = 60
DEBUG = True
interval = 10




def main():
    with open("logs/temperatures.csv", 'r') as f:
        reader = csv.DictReader(f, fieldnames=['date', 'time', 'temperature'])
        next(f)
        temps = list()
        times = list()
        prev_time = None
    
        rows = list(reader)
        firstRow = rows[0]
        times.append(firstRow['time'])
        temps.append(firstRow['temperature']) 
        for row in rows:       
            timestamp = row['time']
            temp = row['temperature']
            curr_time = datetime.strptime(timestamp, "%H:%M:%S")
            
            if(prev_time is None):
                prev_time = curr_time


            
            diff = curr_time - prev_time
            # if(DEBUG):
            #     print(prev_time)
            #     print(curr_time)
            #     print(diff.seconds)

            if(diff.seconds > MIN*interval -5 or diff.seconds > MIN*interval):
                times.append(timestamp)
                temps.append(temp)     
                prev_time = curr_time

        if(DEBUG):
            print("*********times***********")
            print(len(times))
            print("*********temps***********")
            print(len(temps))


def large_csv():
    with open("logs/large.csv", 'a') as f:
        writer = csv.DictWriter(f, fieldnames=['date', 'time', 'temperature'])
        writer.writeheader()
        currentTime = timedelta(2018,1,1)
        
        for x in range(500000):

            date = time.strftime("%Y-%m-%d")
            currentTime = currentTime + timedelta(0,120)
            hours, remainder = divmod(currentTime.seconds, 3600)
            minutes, seconds = divmod(remainder, 60)

            timeStr = '%d:%02d:%02d' %(hours, minutes, seconds)
            newItem = {'date': date, 'time': timeStr, 'temperature': 18.5}
            
            writer.writerow(newItem)


if __name__ == '__main__':
    start_time = time.time()
    # main()
    # large_csv()
    print("--- %s seconds ---" % (time.time() - start_time))
    